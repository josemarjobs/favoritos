class AddPageTitleToLinks < ActiveRecord::Migration
  def change
    add_column :links, :page_title, :string
  end
end
