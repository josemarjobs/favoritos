# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

jQuery ->
	imgs = ($ 'img[src$="loading.gif"]')
	imgCount = imgs.length

	getImage = (img, cb) ->
		url = "#{$(img).data('link-url')}.json"
		$.get url, (link) ->
			console.log link.thumb
			regex = /loading.gif$/
			if regex.test(link.thumb)
				cb(0)
			else
				cb(1, img, link)

	changeImage = (result, img, link) ->
		if result is 1
			imgCount -= 1
			if $('#title_' + link.id).text() is ""
				$('#title_' + link.id).text(link.page_title)
			$(img).attr("src", link.thumb)
			clearInterval interval if imgCount is 0

	callback = -> 
		console.log($(imgs[imgCount - 1]).data('link-url'))
		getImage imgs[imgCount - 1], changeImage

	if imgCount > 0
		interval = setInterval callback, 1000

	($ "#new_link").blur (evt) ->
		if $(this).val().trim() is "http://"
			$(this).val("")

	($ "#new_link").focus (evt) ->
		if $(this).val().trim() is ""
			$(this).val("http://")