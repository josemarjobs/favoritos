class LinksController < ApplicationController
  respond_to :html, :json

  def show
    @link = Link.find(params[:id])
    respond_with @link
  end

  # POST /links
  # POST /links.json
  def create
    @link = current_user.links.build(url: params[:link][:url])
    if @link.register
      redirect_to root_path, notice: 'Link was successfully created.'
    else
      redirect_to root_path, notice: @link.errors.full_messages.join(" and ")
    end
  end

  def edit
    @link = Link.find(params[:id])
  end

  def update
    @link = Link.find(params[:id])
    if @link.update_and_reload(link_params)
      redirect_to root_path, notice: "Link was successfully updated."
    else
      render action: "edit"
    end
  end

  def destroy
    link = current_user.links.find(params[:id])
    link.destroy
    redirect_to root_path, notice: 'Link deleted.'
  end

  private
  
  def link_params
    params.require(:link).permit(:url, :description, :page_title, :tag_names)
  end
end
