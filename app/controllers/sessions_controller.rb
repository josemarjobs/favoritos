class SessionsController < ApplicationController
  def new
  end

  def create
  	user = User.find_by(email: params[:session][:email])
  	if user && user.authenticate(params[:session][:password])
  		sign_in user
  		redirect_to root_path, notice: "Logged in"
  	else
  		redirect_to new_session_path, notice: "Wrong username or password"
  	end
  end

  def destroy
  	session[:user_id] = nil
  	redirect_to root_path, notice: "Logged out"
  end
end
