class LinkJob < Struct.new(:id)
	def perform
		link = Link.find(id)
		link.get_title_and_image
	end
end