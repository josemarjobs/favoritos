module ApplicationHelper

	def current_user_can_edit(link)
		return true if current_user && current_user == link.user
		return false
	end

	def current_user_can_post_to_this_page(user)
		return current_user == user
	end

end
