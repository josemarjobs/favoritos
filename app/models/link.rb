class Link < ActiveRecord::Base
	attr_writer :tag_names

	has_many :taggings, dependent: :destroy
	has_many :tags, through: :taggings
	
	belongs_to :user
	URL_REGEX = /\A(http|https):\/\/|[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,6}(:[0-9]{1,5})?(\/.*)?\z/
	validates :url, presence: true, format: {with: URL_REGEX}

	has_attached_file :image, styles: {
		thumb: "200x200>"
	}, :default_url => "/assets/:style/loading.gif"

	default_scope -> {order("created_at DESC")}

	after_save :create_tags

	def tag_names
		@tag_names || tags.map(&:name).join(', ')
	end

	def register
		return false unless save
		# get_title_and_image
		create_job
		true
	end

	def update_and_reload(params)
		self.url = params[:url]
		self.page_title = params[:page_title]
		self.description = params[:description]
		self.tag_names = params[:tag_names]
		save
		# get_title_and_image
		create_job
	end

	def create_job
		Delayed::Job.enqueue(LinkJob.new(id), run_at: Time.now)
	end

	def get_title_and_image
		save_page_title(self.id)
		save_page_image(self.id)
	end

	private

	def create_tags
		if @tag_names
			self.tags = @tag_names.split(/,\s*/).map do |tag|
				Tag.find_or_create_by(name: tag)
			end
		end
	end

	def save_page_title(id)
		link = Link.find(id)
		return unless link.page_title.nil? or link.page_title.empty?
		link.page_title = get_page_title(link.url)
		link.save
	end

	def save_page_image(id)
		link = Link.find(id)
		file = create_temp_file(imgkit(link.url))
		link.image = file
		link.save
		file.unlink
		true
  end

  def get_page_title(url)
  	agent = Mechanize.new
		page = agent.get(url)
		page.title
  end

  def create_temp_file(img)
  	file = Tempfile.new(["template_#{id}", 'png'], 'tmp',
		                     :encoding => 'ascii-8bit')
		file.write(img)
		file.flush
		file
  end

  def imgkit(url)
  	kit = IMGKit.new(url)
		kit.to_img(:png)
  end
end
