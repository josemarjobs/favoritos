class User < ActiveRecord::Base
	has_many :links, dependent: :destroy
	
	has_secure_password
	validates :username, presence: true, length: {minimum: 4}
	validates :email, 
						presence: true,
						format: {:with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i}

	after_create :send_user_mailer

	def send_user_mailer
		Delayed::Job.enqueue(MailerJob.new(id), run_at: Time.now)
	end
end
