json.extract! @link, :id, :url, :image, :user_id, :page_title, :created_at, :updated_at
json.thumb @link.image.url(:thumb)
