require 'spec_helper'

feature "View Homepage" do
  scenario "user sees relevant information" do
    visit root_path

    expect(page).to have_css 'title', text: 'Favoritos'
    expect(page).to have_css '[data-role="links"]'
  end
end