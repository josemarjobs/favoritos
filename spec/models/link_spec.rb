require 'spec_helper'

describe Link do
  describe "#tag_names" do
  	it "return a list of tags separeted by commas" do
  		link = Link.create!(url: "http://kindelbit.com", tag_names: "kindelbit, java, empresa")
  		newLink = Link.find(link.id)
  		expect(newLink.tag_names).to eq "kindelbit, java, empresa"
	  end  
  end

  describe "#tag_names=" do
    it "creates tags from tag_names separeted with comma and space" do
    	link = Link.create(url: "http://kindelbit.com", tag_names: "kindelbit, java, empresa")

    	expect(link.tags.size).to eq 3
    	expect(link.tags.first.name).to eq 'kindelbit'
    	expect(link.tags.last.name).to eq 'empresa'
    end

    it "creates tags from tag_names separeted with comma" do
    	link = Link.create(url: "http://kindelbit.com", tag_names: "kindelbit,java,empresa")

    	expect(link.tags.size).to eq 3
    	expect(link.tags.first.name).to eq 'kindelbit'
    	expect(link.tags.last.name).to eq 'empresa'
    end

    it "creates tags from tag_names separeted with comma and variable spaces" do
    	link = Link.create(url: "http://kindelbit.com", 
    										tag_names: "kindelbit,  java,empresa, nossa")

    	expect(link.tags.size).to eq 4
    	expect(link.tags.first.name).to eq 'kindelbit'
    	expect(link.tags.last.name).to eq 'nossa'
    end

    it "does not create tag with names already taken" do
    	link = Link.create(url: "http://kindelbit.com", 
    										tag_names: "kindelbit,  java,empresa, nossa")

    	link2 = Link.create(url: "http://kindelbit.com", 
    										tag_names: "kindelbit,  java, aaa")

    	expect(Tag.all.size).to eq 5
    	expect(link.tags.first.name).to eq 'kindelbit'
    	expect(link2.tags.first.name).to eq 'kindelbit'
    	expect(Tag.find_by(name: 'kindelbit').links.size).to eq 2
    end


  end
end
