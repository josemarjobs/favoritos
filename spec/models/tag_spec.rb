require 'spec_helper'

describe Tag do
  
  it "does not create tags without the name" do
  	tag = Tag.new(name: nil)
  	expect(tag).not_to be_valid
  	expect(tag).to have(1).errors_on(:name)
  end

end
